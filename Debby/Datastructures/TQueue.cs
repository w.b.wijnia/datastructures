﻿using System;

using Algos.Datastructures;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Debby.Datastructures
{
    [TestClass]
    public class TQueue
    {

        [TestMethod]
        public void EmptyAtStart()
        {
            Queue<int> structure = new Queue<int>();
            Assert.IsTrue(structure.Empty());
        }

        [TestMethod]
        public void Sequence1()
        {
            Queue<int> structure = new Queue<int>();

            structure.Push(1);
            structure.Push(2);

            int result = structure.Pop();

            Assert.AreEqual(1, result);
        }

        [TestMethod]
        public void Sequence2()
        {
            Queue<int> structure = new Queue<int>();

            structure.Push(1);
            structure.Push(2);
            structure.Push(3);
            structure.Push(4);
            structure.Push(5);

            Assert.AreEqual(1, structure.Pop());
            Assert.AreEqual(2, structure.Pop());
            Assert.AreEqual(3, structure.Pop());
            Assert.AreEqual(4, structure.Pop());
            Assert.AreEqual(5, structure.Pop());

            Assert.IsTrue(structure.Empty());
        }

        [TestMethod]
        public void Sequence3()
        {
            Queue<int> structure = new Queue<int>();

            structure.Push(1);
            Assert.AreEqual(1, structure.Pop());
            structure.Push(2);
            Assert.AreEqual(2, structure.Pop());
            structure.Push(3);
            Assert.AreEqual(3, structure.Pop());
            structure.Push(4);
            Assert.AreEqual(4, structure.Pop());
            structure.Push(5);
            Assert.AreEqual(5, structure.Pop());

            Assert.IsTrue(structure.Empty());
        }

        [TestMethod]
        public void ExceptionOnEmptyPop()
        {
            Queue<int> structure = new Queue<int>();
            Assert.ThrowsException<NullReferenceException>(() => structure.Pop());
        }
    }
}