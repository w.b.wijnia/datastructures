﻿using System;
using System.Text;

using Algos.Datastructures;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Debby.Datastructures
{
    [TestClass]
    public class TStack
    {

        [TestMethod]
        public void EmptyAtStart()
        {
            Stack<int> structure = new Stack<int>();
            Assert.IsTrue(structure.Empty());
        }

        [TestMethod]
        public void Sequence1()
        {
            Stack<int> structure = new Stack<int>();

            structure.Push(1);
            structure.Push(2);

            int result = structure.Pop();

            Assert.AreEqual(2, result);
        }

        [TestMethod]
        public void Sequence2()
        {
            Stack<int> structure = new Stack<int>();

            structure.Push(1);
            structure.Push(2);
            structure.Push(3);
            structure.Push(4);
            structure.Push(5);

            Assert.AreEqual(5, structure.Pop());
            Assert.AreEqual(4, structure.Pop());
            Assert.AreEqual(3, structure.Pop());
            Assert.AreEqual(2, structure.Pop());
            Assert.AreEqual(1, structure.Pop());

            Assert.IsTrue(structure.Empty());
        }

        [TestMethod]
        public void Sequence3()
        {
            Stack<int> structure = new Stack<int>();

            structure.Push(1);
            Assert.AreEqual(1, structure.Pop());
            structure.Push(2);
            Assert.AreEqual(2, structure.Pop());
            structure.Push(3);
            Assert.AreEqual(3, structure.Pop());
            structure.Push(4);
            Assert.AreEqual(4, structure.Pop());
            structure.Push(5);
            Assert.AreEqual(5, structure.Pop());

            Assert.IsTrue(structure.Empty());
        }

        [TestMethod]
        public void ExceptionOnEmptyPop()
        {
            Stack<int> structure = new Stack<int>();
            Assert.ThrowsException<NullReferenceException>(() => structure.Pop());
        }
    }
}