﻿using System;
using System.Collections.Generic;
using System.Text;

using Algos.Sort.Array;
using Algos.Datastructures.Priority;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Debby.Datastructures.Priority
{
    public abstract class TPriority
    {

        protected PriorityQueue<int> priority;

        [TestMethod]
        public void EmptyAtStart()
        {
            Assert.IsTrue(priority.IsEmpty());
        }

        [TestMethod]
        public void OneInsertPeekExtract()
        {
            int value = 1; 
            priority.Insert(value);
            Assert.AreEqual(value, priority.Peek());
            Assert.AreEqual(value, priority.Extract());
            Assert.IsTrue(priority.IsEmpty());
        }

        [TestMethod]
        public void SmallSorting()
        {
            Sorting<int> count = new Merge<int>();

            int[] values = new int[] { 3, 7, 4, 1, 9, -1 };

            for (int j = 0; j < values.Length; j++)
                priority.Insert(values[j]);

            int[] sortedValues = count.Sort(values);

            for (int j = 0; j < sortedValues.Length; j++)
                Assert.AreEqual(sortedValues[j], priority.Extract());

            Assert.IsTrue(priority.IsEmpty());
        }
    }
}