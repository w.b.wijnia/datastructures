﻿using System;
using System.Collections.Generic;
using System.Text;

using Algos.Datastructures.Priority;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Debby.Datastructures.Priority
{
    [TestClass]
    public class THeap : TPriority
    {
        public THeap()
        {
            this.priority = new PrioHeap<int>();
        }
    }
}
