﻿using Algos.Search.Array;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Debby.Search.Array
{
    [TestClass]
    public class TLinear : TSearching
    {
        public TLinear()
        {
            this.searching = new Linear<int>();
        }
    }
}
