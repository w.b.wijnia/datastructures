﻿
using Algos.Search.Array;

using System.Text;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Debby.Search.Array
{
    public class TSearching
    {
        protected Searching<int> searching;

        [TestMethod]
        public void NoElements()
        {
            int targ = 5;
            int[] ints = new int[] { };

            bool member = searching.Member(ints, targ);
            int index = searching.Search(ints, targ);

            Assert.IsFalse(member);
            Assert.AreEqual(-1, index);
        }

        [TestMethod]
        public void OneElementValid()
        {
            int targ = 5;
            int[] ints = new int[] { 5 };

            bool member = searching.Member(ints, targ);
            int index = searching.Search(ints, targ);

            Assert.IsTrue(member);
            Assert.AreEqual(0, index);
        }

        [TestMethod]
        public void OneElementInvalid()
        {
            int targ = 5;
            int[] ints = new int[] { 4 };

            bool member = searching.Member(ints, targ);
            int index = searching.Search(ints, targ);

            Assert.IsFalse(member);
            Assert.AreEqual(-1, index);
        }

        [TestMethod]
        public void AtStart()
        {
            int targ = 5;
            int[] ints = new int[] { 5,6,7,8,9 };

            bool member = searching.Member(ints, targ);
            int index = searching.Search(ints, targ);

            Assert.IsTrue(member);
            Assert.AreEqual(0, index);
        }

        [TestMethod]
        public void AtEnd()
        {
            int targ = 5;
            int[] ints = new int[] { 1,2,3,4,5 };

            bool member = searching.Member(ints, targ);
            int index = searching.Search(ints, targ);

            Assert.IsTrue(member);
            Assert.AreEqual(4, index);
        }

        [TestMethod]
        public void SecondElement()
        {
            if (this.GetType() == typeof(TSearching))
                return;

            int targ = 2;
            int[] ints = new int[] { 1, 2, 3, 4, 5 };

            bool member = searching.Member(ints, targ);
            int index = searching.Search(ints, targ);

            Assert.IsTrue(member);
            Assert.AreEqual(1, index);
        }

        [TestMethod]
        public void ThirdElement()
        {
            if (this.GetType() == typeof(TSearching))
                return;

            int targ = 3;
            int[] ints = new int[] { 1, 2, 3, 4, 5 };

            bool member = searching.Member(ints, targ);
            int index = searching.Search(ints, targ);

            Assert.IsTrue(member);
            Assert.AreEqual(2, index);
        }

        [TestMethod]
        public void FourthElement()
        {
            if (this.GetType() == typeof(TSearching))
                return;

            int targ = 4;
            int[] ints = new int[] { 1, 2, 3, 4, 5 };

            bool member = searching.Member(ints, targ);
            int index = searching.Search(ints, targ);

            Assert.IsTrue(member);
            Assert.AreEqual(3, index);
        }
    }
}