﻿using Algos.Search.Array;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Debby.Search.Array
{
    [TestClass]
    public class TBinary : TSearching
    {
        public TBinary()
        {
            this.searching = new Binary<int>();
        }
    }
}
