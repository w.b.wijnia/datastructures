﻿
using Algos.Search;

using System.Text;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Debby.Search.String
{

    public class TMatching
    {
        protected Matching matching;

        [TestMethod]
        public void SingletonPattern()
        {
            string st = "Hello. This is a test.";
            string sp = "s";

            byte[] bt = Encoding.UTF8.GetBytes(st);
            byte[] bp = Encoding.UTF8.GetBytes(sp);

            List<int> shifts = matching.Match(bt, bp);
            Assert.AreEqual(3, shifts.Count);
        }

        [TestMethod]
        public void EmptyPattern()
        {
            string st = "Hello. This is a test.";
            string sp = "";

            byte[] bt = Encoding.UTF8.GetBytes(st);
            byte[] bp = Encoding.UTF8.GetBytes(sp);

            List<int> shifts = matching.Match(bt, bp);
            Assert.AreEqual(0, shifts.Count);
        }

        [TestMethod]
        public void ShiftValidAll()
        {
            string st = "aaaa";
            string sp = "a";

            byte[] bt = Encoding.UTF8.GetBytes(st);
            byte[] bp = Encoding.UTF8.GetBytes(sp);

            List<int> shifts = matching.Match(bt, bp);
            Assert.AreEqual(4, shifts.Count);
        }

        [TestMethod]
        public void EmptyText()
        {
            string st = "";
            string sp = "a";

            byte[] bt = Encoding.UTF8.GetBytes(st);
            byte[] bp = Encoding.UTF8.GetBytes(sp);

            List<int> shifts = matching.Match(bt, bp);
            Assert.AreEqual(0, shifts.Count);
        }

        [TestMethod]
        public void ShiftValidFirst()
        {
            string st = "abbc";
            string sp = "a";

            byte[] bt = Encoding.UTF8.GetBytes(st);
            byte[] bp = Encoding.UTF8.GetBytes(sp);

            List<int> shifts = matching.Match(bt, bp);
            Assert.AreEqual(1, shifts.Count);
        }

        [TestMethod]
        public void ShiftValidLast()
        {
            string st = "abbc";
            string sp = "c";

            byte[] bt = Encoding.UTF8.GetBytes(st);
            byte[] bp = Encoding.UTF8.GetBytes(sp);

            List<int> shifts = matching.Match(bt, bp);
            Assert.AreEqual(1, shifts.Count);
        }


        [TestMethod]
        public void PatternBiggerThanText()
        {
            string st = "a";
            string sp = "aa";

            byte[] bt = Encoding.UTF8.GetBytes(st);
            byte[] bp = Encoding.UTF8.GetBytes(sp);

            List<int> shifts = matching.Match(bt, bp);
            Assert.AreEqual(0, shifts.Count);
        }

        [TestMethod]
        public void WikipediaArticle1()
        {
            string st = "'Netherlands' literally means 'lower countries', referring to its low and flat topography, with only about 50% of its land exceeding 1 metre (3 ft 3 in) above sea level and nearly 17% being below sea level.[16] Most of the areas below sea level, known as polders, are the result of land reclamation beginning in the 16th century. With a population of 17.25 million living within a total area of roughly 41,500 square kilometres (16,000 sq mi)—of which the land area is 33,700 square kilometres (13,000 sq mi)—the Netherlands is one of the most densely populated countries in the world. Nevertheless, it is the world's second-largest exporter of food and agricultural products after the United States, owing to its fertile soil, mild climate, and intensive agriculture.";
            string sp = "to";

            byte[] bt = Encoding.UTF8.GetBytes(st);
            byte[] bp = Encoding.UTF8.GetBytes(sp);

            List<int> shifts = matching.Match(bt, bp);
            Assert.AreEqual(4, shifts.Count);
        }

        [TestMethod]
        public void WikipediaArticle2()
        {
            string st = "'Netherlands' literally means 'lower countries', referring to its low and flat topography, with only about 50% of its land exceeding 1 metre (3 ft 3 in) above sea level and nearly 17% being below sea level.[16] Most of the areas below sea level, known as polders, are the result of land reclamation beginning in the 16th century. With a population of 17.25 million living within a total area of roughly 41,500 square kilometres (16,000 sq mi)—of which the land area is 33,700 square kilometres (13,000 sq mi)—the Netherlands is one of the most densely populated countries in the world. Nevertheless, it is the world's second-largest exporter of food and agricultural products after the United States, owing to its fertile soil, mild climate, and intensive agriculture.";
            string sp = "population";

            byte[] bt = Encoding.UTF8.GetBytes(st);
            byte[] bp = Encoding.UTF8.GetBytes(sp);

            List<int> shifts = matching.Match(bt, bp);
            Assert.AreEqual(1, shifts.Count);
        }

        // for TCheckSum: first value fails.

        [TestMethod()]
        [DeploymentItem("Data/SupremeCommanderGlobalLua.txt")]
        public void SupremeCommanderGlobalLua1()
        {
            string st = System.IO.File.ReadAllText("Data/SupremeCommanderGlobalLua.txt");
            string sp = "INFO";

            byte[] bt = Encoding.UTF8.GetBytes(st);
            byte[] bp = Encoding.UTF8.GetBytes(sp);

            List<int> shifts = matching.Match(bt, bp);
            Assert.AreEqual(68182, shifts.Count);
        }

        [TestMethod()]
        [DeploymentItem("Data/SupremeCommanderGlobalLua.txt")]
        public void SupremeCommanderGlobalLua2()
        {
            string st = System.IO.File.ReadAllText("Data/SupremeCommanderGlobalLua.txt");
            string sp = "EngineerExpansionBuildersSmall";

            byte[] bt = Encoding.UTF8.GetBytes(st);
            byte[] bp = Encoding.UTF8.GetBytes(sp);

            List<int> shifts = matching.Match(bt, bp);
            Assert.AreEqual(51, shifts.Count);
        }

        [TestMethod()]
        public void WorstCase1()
        {
            string st = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            string sp = "aaaaaaab";

            byte[] bt = Encoding.UTF8.GetBytes(st);
            byte[] bp = Encoding.UTF8.GetBytes(sp);

            List<int> shifts = matching.Match(bt, bp);
            Assert.AreEqual(0, shifts.Count);
        }

        [TestMethod()]
        public void WorstCase2()
        {
            string st = new string('a', 250000);
            string sp = "aaaaaaaaaaaaaaaaaaaaaaaaaaaab";

            byte[] bt = Encoding.UTF8.GetBytes(st);
            byte[] bp = Encoding.UTF8.GetBytes(sp);

            List<int> shifts = matching.Match(bt, bp);
            Assert.AreEqual(0, shifts.Count);
        }
    }
}
