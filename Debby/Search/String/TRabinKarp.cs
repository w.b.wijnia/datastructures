
using Algos.Search;

using System.Text;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Debby.Search.String
{
    [TestClass]
    public class TRabinKarp : TMatching
    {
        public TRabinKarp()
        {
            this.matching = new RabinKarp();
        }
    }
}
