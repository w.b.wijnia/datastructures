﻿using System;
using System.Collections.Generic;
using System.Text;

using Algos.Sort.Array;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Debby.Sort.Array
{
    public abstract class TSorting
    {

        protected Sorting<int> sorting;

        [TestMethod]
        public void NoElements()
        {
            if (this.GetType() == typeof(TSorting))
                return;

            int[] elements = new int[] { };
            int n = elements.Length;

            elements = sorting.Sort(elements);

            Assert.IsTrue(elements.Length == n);
        }

        [TestMethod]
        public void OneElement()
        {
            if (this.GetType() == typeof(TSorting))
                return;

            Random rng = new Random();

            int element = rng.Next();
            int[] elements = new int[] { element };
            int n = elements.Length;

            elements = sorting.Sort(elements);

            Assert.IsTrue(elements.Length == n);
            Assert.IsTrue(elements[0] == element);

        }

        [TestMethod]
        public void InOrder()
        {
            if (this.GetType() == typeof(TSorting))
                return;

            int[] elements = new int[] { 1, 2, 3, 4, 5 };
            int n = elements.Length;

            elements = sorting.Sort(elements);

            Assert.IsTrue(elements.Length == n);
            Assert.IsTrue(IsSorted(elements));
        }

        [TestMethod]
        public void DuplicateElements()
        {
            if (this.GetType() == typeof(TSorting))
                return;

            int[] elements = new int[] { 5, 3, 4, 1, 3, 4, 1, 2 };
            int n = elements.Length;

            elements = sorting.Sort(elements);

            Assert.IsTrue(elements.Length == n);
            Assert.IsTrue(IsSorted(elements));
        }

        [TestMethod]
        public void DuplicateElementsInOrder()
        {
            if (this.GetType() == typeof(TSorting))
                return;

            int[] elements = new int[] { 1, 2, 3, 3, 4, 4, 5 };
            int n = elements.Length;

            elements = sorting.Sort(elements);

            Assert.IsTrue(elements.Length == n);
            Assert.IsTrue(IsSorted(elements));
        }

        [TestMethod]
        public void ReverseOrder()
        {
            if (this.GetType() == typeof(TSorting))
                return;

            int[] elements = new int[] { 5, 4, 3, 2, 1 };
            int n = elements.Length;

            elements = sorting.Sort(elements);

            Assert.IsTrue(elements.Length == n);
            Assert.IsTrue(IsSorted(elements));
        }

        [TestMethod]
        public void Preset3()
        {
            if (this.GetType() == typeof(TSorting))
                return;

            int[] elements = new int[] { 1, 5, 4, 3, 2 };
            int n = elements.Length;

            elements = sorting.Sort(elements);

            Assert.IsTrue(elements.Length == n);
            Assert.IsTrue(IsSorted(elements));
        }

        [TestMethod]
        public void Preset4()
        {
            if (this.GetType() == typeof(TSorting))
                return;

            int[] elements = new int[] { 8, 48, 193, 1844, 19, 182, 1, 39, 49, 06, 28, 16, 39, 69, 48 };
            int n = elements.Length;

            elements = sorting.Sort(elements);

            Assert.IsTrue(elements.Length == n);
            Assert.IsTrue(IsSorted(elements));
        }

        [TestMethod]
        public void RandomSmall()
        {
            if (this.GetType() == typeof(TSorting))
                return;

            Random rng = new Random(0);
            int[] elements = Generate(rng, 1000, 0, 5000);
            int n = elements.Length;

            elements = sorting.Sort(elements);

            Assert.IsTrue(elements.Length == n);
            Assert.IsTrue(IsSorted(elements));
        }

        [TestMethod]
        public void RandomMedium()
        {
            if (this.GetType() == typeof(TSorting))
                return;

            Random rng = new Random(0);
            int[] elements = Generate(rng, 10000, 0, 50000);
            int n = elements.Length;

            elements = sorting.Sort(elements);

            Assert.IsTrue(elements.Length == n);
            Assert.IsTrue(IsSorted(elements));
        }

        [TestMethod]
        public void RandomBig()
        {
            if (this.GetType() == typeof(TSorting))
                return;

            Random rng = new Random(0);
            int[] elements = Generate(rng, 25000, 0, 125000);
            int n = elements.Length;

            elements = sorting.Sort(elements);

            Assert.IsTrue(elements.Length == n);
            Assert.IsTrue(IsSorted(elements));
        }

        /// <summary>
        /// Checks whether the given array is sorted.
        /// </summary>
        private bool IsSorted(int[] array)
        {
            int n = array.Length;

            if (n <= 1)
                return true;

            for (int j = 1; j < n; j++)
                if (array[j] < array[j - 1])
                    return false;

            return true;
        }

        /// <summary>
        /// Generates an array of size n, with elements between the bounds of l and h given the random generator rng.
        /// </summary>
        private int[] Generate(Random rng, int n, int l, int h)
        {
            int[] output = new int[n];

            for (int j = 0; j < n; j++)
                output[j] = rng.Next(l, h);

            return output;
        }
    }
}