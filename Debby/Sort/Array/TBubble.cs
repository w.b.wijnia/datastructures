﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Algos.Sort.Array;

namespace Debby.Sort.Array
{
    [TestClass]
    public class TBubble : TSorting
    {
        public TBubble()
        {
            this.sorting = new BubbleSort<int>();
        }
    }
}

