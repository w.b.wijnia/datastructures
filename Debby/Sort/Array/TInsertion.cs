﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Algos.Sort.Array;

namespace Debby.Sort.Array
{
    [TestClass]
    public class TInsertion : TSorting
    {
        public TInsertion()
        {
            this.sorting = new Insertion<int>();
        }
    }
}

