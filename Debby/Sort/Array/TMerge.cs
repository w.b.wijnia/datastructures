﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Algos.Sort.Array;

namespace Debby.Sort.Array
{
    [TestClass]
    public class TMerge : TSorting
    {
        public TMerge()
        {
            this.sorting = new Merge<int>();
        }
    }
}

