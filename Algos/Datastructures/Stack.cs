﻿using System;
using System.Collections;

namespace Algos.Datastructures
{
    public class Stack<T> : ICloneable, System.Collections.ICollection
    {
        /// <summary>
        /// Represents the next element on the stack.
        /// </summary>
        private Node head;

        public int Count => throw new NotImplementedException();

        // https://docs.microsoft.com/en-us/dotnet/api/system.collections.icollection.issynchronized?view=netframework-4.7.2
        public bool IsSynchronized => throw new NotImplementedException();

        // https://docs.microsoft.com/en-us/dotnet/api/system.collections.icollection.syncroot?view=netframework-4.7.2
        public object SyncRoot => throw new NotImplementedException();

        /// <summary>
        /// Checks whether this stack is empty.
        /// </summary>
        /// <returns></returns>
        public bool Empty()
        {
            return head == null; 
        }

        /// <summary>
        /// Pushes the given element onto this stack.
        /// </summary>
        public void Push(T element)
        {
            if (head == null)
            {
                head = new Node(element);
            }
            else
            {
                head = new Node(element, head);
            }
        }

        /// <summary>
        /// Pops the top element from the stack. Throws an exception when the stack is empty.
        /// </summary>
        /// <returns></returns>
        public T Pop()
        {
            // pop the tail.
            Node oldHead = head;
            head = oldHead.next;

            // return its element.
            T element = oldHead.element;
            return element;
        }

        public object Clone()
        {
            throw new NotImplementedException();
        }

        public void CopyTo(Array array, int index)
        {
            throw new NotImplementedException();
        }

        public IEnumerator GetEnumerator()
        {
            throw new NotImplementedException();
        }

        private class Node
        {
            /// <summary>
            /// Represents the element this node holds.
            /// </summary>
            public T element;

            /// <summary>
            /// Represents the next and previous node.
            /// </summary>
            public Node next;

            /// <summary>
            /// Constructs a node with the given element.
            /// </summary>
            public Node(T element)
            {
                this.element = element;
            }

            /// <summary>
            /// Constructs a node with the given element and connects the nodes together.
            /// </summary>
            public Node(T element, Node next)
            {
                this.element = element;
                this.next = next;
            }
        }
    }
}
