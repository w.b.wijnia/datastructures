﻿using System;

namespace Algos.Datastructures
{
    public class LinkedList<S> where S : IEquatable<S>
    {

        /// <summary>
        /// Represents the number of nodes in this list.
        /// </summary>
        private int count = 0;

        /// <summary>
        /// Represents the head and tail element of this list.
        /// </summary>
        LinkedListNode<S> head, tail;

        /// <summary>
        /// Adds the given key, element combination to the set.
        /// </summary>
        public void PushHead(S satalite)
        {
            LinkedListNode<S> node = new LinkedListNode<S>(satalite);

            switch (count)
            {
                case 0:
                    // the added entry is both the head and the tail. 
                    head = node;
                    tail = node;
                    count = count + 1;
                    break;

                default:
                    // the added entry becomes the new head.
                    node.next = head;
                    head.prev = node;
                    head = node;
                    break;
            }
        }

        /// <summary>
        /// Deletes the given node from the set.
        /// </summary>
        /// <param name="node"></param>
        public void Delete(LinkedListNode<S> node)
        {
            LinkedListNode<S> left, right;
            left = node.prev;
            right = node.next;

            if (left != null)
                left.next = right;

            if (right != null)
                right.prev = left;
        }

        /// <summary>
        /// Pops the head. Throws an error when the set is empty.
        /// </summary>
        /// <returns></returns>
        public S PopHead()
        {
            if (Empty())
                throw new Exception("Linked list is empty.");

            LinkedListNode<S> node = head;
            node.next.prev = null;
            head = node.next;
            return node.satalite;
        }

        /// <summary>
        /// Pops the tail. Throws an error when the set is empty.
        /// </summary>
        /// <returns></returns>
        public S PopTail()
        {
            if (Empty())
                throw new Exception("Linked list is empty.");

            LinkedListNode<S> node = tail;
            node.prev.next = null;
            tail = node.prev;

            return node.satalite;
        }

        /// <summary>
        /// Searches through the set to find a node with the given key. Returns null if the node cannot be found.
        /// </summary>
        /// <param name="key">The key to search for.</param>
        /// <returns></returns>
        public LinkedListNode<S> Search(S key)
        {
            LinkedListNode<S> node = head;
            while(node != null && !node.satalite.Equals(key))
            {
                node = node.next;
            }

            return node;
        }

        /// <summary>
        /// Represents whether the set is empty or not.
        /// </summary>
        /// <returns></returns>
        public bool Empty()
        {
            return count == 0;
        }
    }
}
