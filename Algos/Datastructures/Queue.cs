﻿using System;

namespace Algos.Datastructures
{
    public class Queue<T>
    {
        /// <summary>
        /// Represents the next element on the queue.
        /// </summary>
        private Node head;

        /// <summary>
        /// Represents the last element on the queue.
        /// </summary>
        private Node tail;

        /// <summary>
        /// Checks whether this stack is empty.
        /// </summary>
        /// <returns></returns>
        public bool Empty()
        {
            return head == null;
        }

        /// <summary>
        /// Pushes the given element onto this stack.
        /// </summary>
        /// <param name="element">The element to push onto the stack.</param>
        public void Push(T element)
        {
            if(head == null)
            {
                head = new Node(element);
                tail = head;
            }
            else
            {
                tail = new Node(element, tail);
            }
        }

        /// <summary>
        /// Pops the top element from the stack. Throws an exception when the stack is empty.
        /// </summary>
        /// <returns></returns>
        public T Pop()
        {
            // pop the tail.
            Node oldHead = head;
            head = oldHead.priv;

            // return its element.
            T element = oldHead.element;
            return element;
        }

        private class Node
        {
            /// <summary>
            /// Represents the element this node holds.
            /// </summary>
            public T element;

            /// <summary>
            /// Represents the next and previous node.
            /// </summary>
            public Node next, priv;

            /// <summary>
            /// Constructs a node with the given element.
            /// </summary>
            public Node(T element)
            {
                this.element = element;
            }

            /// <summary>
            /// Constructs a node with the given element and connects the nodes together.
            /// </summary>
            public Node (T element, Node next)
            {
                this.element = element;
                this.next = next;
                next.priv = this;
            }
        }
    }
}
