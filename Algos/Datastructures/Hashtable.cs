﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Algos.Datastructures
{
    public class Hashtable<S> where S : IEquatable<S>
    {

        /// <summary>
        /// The number of entries within this hashtable.
        /// </summary>
        int m;

        /// <summary>
        /// The entries within this hastable.
        /// </summary>
        LinkedList<S>[] entries;

        /// <summary>
        /// Constructs a hash table of a fixed size of 701.
        /// </summary>
        public Hashtable()
        {
            this.m = 701;
            ConstructTable(m);
        }

        /// <summary>
        /// Constructs a hash table of the given size.
        /// </summary>
        /// <param name="m">The size of the hastable.</param>
        public Hashtable(int m)
        {
            if (m == 0)
            {
                throw new ArgumentException("Cannot initialize a hashtable of size: " + m.ToString());
            }

            this.m = m;
            ConstructTable(m);
        }

        /// <summary>
        /// Constructs the hashtable.
        /// </summary>
        /// <param name="m">The size of the hashtable to construct.</param>
        private void ConstructTable(int m)
        {
            this.entries = new LinkedList<S>[m];

            for (int j = 0; j < m; j++)
                entries[m] = new LinkedList<S>();
        }

        /// <summary>
        /// Adds the given satalite data to the hashtable.
        /// </summary>
        /// <param name="data"></param>
        public void Insert(S data)
        {
            int hash = ComputeHash(data);
            entries[hash].PushHead(data);
        }

        /// <summary>
        /// Removes the given satalite data from the hashtable - if present.
        /// </summary>
        /// <param name="data"></param>
        public void Delete(S data)
        {
            int hash = ComputeHash(data);
            LinkedListNode<S> node = entries[hash].Search(data);

            if (node != null)
            {
                entries[hash].Delete(node);
            }
        }

        /// <summary>
        /// Checks whether the given satalite data is in the hashtable.
        /// </summary>
        /// <param name="data"></param>
        public bool Member(S data)
        {
            int hash = ComputeHash(data);
            return entries[hash].Search(data) != null;
        }

        private int ComputeHash(S data)
        {
            int code = data.GetHashCode();
            int hash = code % m;

            return hash;
        }
    }
}