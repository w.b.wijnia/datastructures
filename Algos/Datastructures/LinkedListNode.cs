﻿using System;

namespace Algos.Datastructures
{
    public class LinkedListNode<S>
    {
        /// <summary>
        /// The element that this entry holds.
        /// </summary>
        public S satalite;

        /// <summary>
        /// Represents the next and previous node in the list.
        /// </summary>
        public LinkedListNode<S> next, prev;

        public LinkedListNode(S satalite)
        {
            this.satalite = satalite;
        }
    }
}
