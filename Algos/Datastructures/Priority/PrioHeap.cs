﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Algos.Datastructures.Priority
{
    public class PrioHeap<T> : PriorityQueue<T> where T : IComparable<T>
    {

        /// <summary>
        /// Represents the heap.
        /// </summary>
        private T[] heap;

        /// <summary>
        /// Represents the tail element of the heap - the last element of the heap.
        /// </summary>
        private int tail = 1;

        /// <summary>
        /// Represents the root element.
        /// </summary>
        private const int root = 1;

        /// <summary>
        /// Constructs an empty heap.
        /// </summary>
        public PrioHeap()
        {
            heap = new T[2];
        }

        /// <summary>
        /// Constructs a heap with the given element at the root.
        /// </summary>
        /// <param name="element"></param>
        public PrioHeap(T element)
        {
            heap = new T[2];
            Insert(element);
        }

        /// <summary>
        /// Constructs a heap from the given elements.
        /// </summary>
        /// <param name="elements"></param>
        public PrioHeap(T[] elements)
        {
            heap = new T[2];

            foreach (T element in elements)
                Insert(element);
        }

        public override void Insert(T element)
        {
            // if we ran out of space - increase the space.
            if (tail == heap.Length)
            {
                IncreaseHeapSize();
            }

            // Adds the element at the end of the heap.
            // Performs a 'bubbleup' operation to restore
            // the properties of the heap.
            heap[tail] = element;
            BubbleUp(tail);
            tail += 1;
        }

        /// <summary>
        /// Retrieves the maximum value from the heap.
        /// </summary>
        /// <returns></returns>
        public override T Extract()
        {
            if(IsEmpty())
            {
                throw new Exception("Heap is empty.");
            }

            // if we have too much space - decrease the space.
            if (tail < heap.Length >> 2)
            {
                DecreaseHeapSize();
            }

            // Retrieves the element from the heap.
            // Places the element at the end of the heap at the top,
            // performs a 'bubbledown' operation to restore
            // the properties of the heap.
            T element = heap[root];
            tail -= 1;
            heap[root] = heap[tail];
            heap[tail] = default(T);
            BubbleDown(root);

            return element;
        }

        public override bool IsEmpty()
        {
            return tail == root;
        }

        /// <summary>
        /// Retrieves the maximum value from the heap without popping it.
        /// </summary>
        /// <returns></returns>
        public override T Peek()
        {
            T element = heap[0];
            return element;
        }

        /// <summary>
        /// Also mentioned as Heapify in literature. Restores the heap properties.
        /// </summary>
        private void BubbleDown(int index)
        {
            // retrieve the smallest child.
            int child = SmallestChild(index);

            // if there is a child, check if we're smaller.
            if (child != -1)
            {
                T iElement = heap[index];
                T cElement = heap[child];

                if (iElement.CompareTo(cElement) > 0)
                {
                    SwapInHeap(index, child);
                    BubbleDown(child);
                }
            }
        }

        /// <summary>
        /// Also mentioned as Rootify in literature. Restores the heap properties.
        /// </summary>
        private void BubbleUp(int index)
        {
            // we're at the root - we cannot go any higher.
            if (index == root)
            {
                return;
            }

            // retrieve the parent.
            int parent = IndexParent(index);

            // check if we're bigger.
            T nElement = heap[index];
            T pElement = heap[parent];

            if (nElement.CompareTo(pElement) < 0)
            {
                SwapInHeap(index, parent);
                BubbleUp(parent);
            }
        }

        /// <summary>
        /// Retrieves the largest child of the given parent. Returns -1 if the parent has no children.
        /// </summary>
        /// <param name="parent"></param>
        /// <returns></returns>
        private int SmallestChild(int parent)
        {
            // compute the indices of the children.
            int a = IndexChildLeft(parent);
            int b = IndexChildRight(parent);

            // check if the left child is there.
            if (a >= tail)
            {
                return -1;
            }

            // check if the right child is there.
            if (b >= tail)
            {
                return a;
            }

            // both children are there.
            if (heap[a].CompareTo(heap[b]) < 0)
            {
                return a;
            }
            else
            {
                return b;
            }
        }

        /// <summary>
        /// Computes the index of the parent.
        /// </summary>
        private int IndexParent(int index)
        {
            return index >> 1;
        }

        /// <summary>
        /// Computes the index of the left child.
        /// </summary>
        private int IndexChildLeft(int index)
        {
            return (index << 1) + 0;
        }

        /// <summary>
        /// Computes the index of the right child.
        /// </summary>
        private int IndexChildRight(int index)
        {
            return (index << 1) + 1;
        }

        /// <summary>
        /// Swaps the two elements within the heap.
        /// </summary>
        /// <param name="array"></param>
        /// <param name="a"></param>
        /// <param name="b"></param>
        private void SwapInHeap(int a, int b)
        {
            T temp = heap[a];
            heap[a] = heap[b];
            heap[b] = temp;
        }

        /// <summary>
        /// Increase the heap in size. Take note: This is an O(n) operation.
        /// </summary>
        private void IncreaseHeapSize()
        {
            T[] newHeap = new T[heap.Length * 2];

            for (int j = 0; j < heap.Length; j++)
                newHeap[j] = heap[j];

            heap = newHeap;
        }

        /// <summary>
        /// Decreases the heap in size. Take note: This is an O(n) operation.
        /// </summary>
        private void DecreaseHeapSize()
        {
            T[] newHeap = new T[heap.Length / 2];

            for (int j = 0; j < newHeap.Length; j++)
                newHeap[j] = heap[j];

            heap = newHeap;
        }
    }
}