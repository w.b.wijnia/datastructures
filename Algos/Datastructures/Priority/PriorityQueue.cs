﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Algos.Datastructures.Priority
{
    public abstract class PriorityQueue<T> where T:IComparable<T>
    {

        public abstract T Peek();

        public abstract bool IsEmpty();

        public abstract T Extract();

        public abstract void Insert(T element);

    }
}
