﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Algos.Search.Array
{
    public class Linear<T> : Searching<T> where T : IEquatable<T>
    {

        /// <summary>
        /// Checks whether the given target is a member of this array.
        /// </summary>
        /// <param name="array">The array to search in.</param>
        /// <param name="target">The target to search for.</param>
        /// <returns></returns>
        public override bool Member(T[] array, T target)
        {
            return Search(array, target) != -1;
        }

        /// <summary>
        /// Returns the index of the first occurence of the given target. Returns -1 if the target is not present.
        /// </summary>
        /// <param name="array">The array to search in.</param>
        /// <param name="target">The target to search for.</param>
        /// <returns></returns>
        public override int Search(T[] array, T target)
        {
            if (array.Length == 0)
                return -1;

            for (int j = 0, l = array.Length; j < l; j++)
            {
                T element = array[j];
                if(element.Equals(target))
                {
                    return j;
                }
            }

            return -1;
        }
    }
}