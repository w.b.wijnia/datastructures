﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Algos.Search.Array
{
    public abstract class Searching<T>
    {

        public abstract bool Member(T[] array, T target);

        public abstract int Search(T[] array, T target);

    }
}