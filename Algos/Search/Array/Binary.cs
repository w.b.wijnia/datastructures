﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Algos.Search.Array
{
    public class Binary<T> : Searching<T> where T : IComparable<T>
    {

        /// <summary>
        /// Checks whether the given target is a member of this array.
        /// </summary>
        /// <param name="array">The array to search in.</param>
        /// <param name="target">The target to search for.</param>
        /// <returns></returns>
        public override bool Member(T[] array, T target)
        {
            return Search(array, target) != -1;
        }

        /// <summary>
        /// Returns the index of the first occurence of the given target. Returns -1 if the target is not present.
        /// </summary>
        /// <param name="array">The array to search in.</param>
        /// <param name="target">The target to search for.</param>
        /// <returns></returns>
        public override int Search(T[] array, T target)
        {
            if (array.Length == 0)
                return -1;

            // the lower and higher bound.
            int l = 0;
            int h = array.Length;
            
            while (true)
            {
                // the middle of the array.
                int m = (l + h) / 2;
                T element = array[m];

                // end condition
                if (l == h - 1)
                {
                    if(element.Equals(target))
                    {
                        return m;
                    }
                    else
                    {
                        return -1;
                    }
                }

                // recursion
                int c = element.CompareTo(target);
                if(c <= 0)
                {
                    l = m;
                }
                else
                {
                    h = m;
                }
            }
        }
    }
}
