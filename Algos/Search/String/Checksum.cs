﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Algos.Search
{
    public class Checksum : Matching
    {
        // Works with a 'slider' to allow us to check for multiple characters at once.
        // t  :  |023|076|099|100|004|032|241|233|075|064|125|184|134|001|...|
        // ts :          |099|100|004|032|241|233|075|064|

        // ps : |100|004|032|
        // ms : |255|255|255|000|000|000|000|000|

        // a mask is required to allow for shorter patterns. Longer patterns are not
        // affected by the mask.
        
        /// <summary>
        /// Searches the given pattern in the given text. The offset for every valid shift (hit) is returned.
        /// </summary>
        /// <param name="text">The text to search through.</param>
        /// <param name="pattern">The pattern to search for.</param>
        public override List<int> Match(byte[] text, byte[] pattern)
        {
            // keeps track of the number of valid shifts found.
            List<int> validShifts = new List<int>();

            // retrieve the (clamped) lengths. The pattern is clamped because only 8 bytes fit in a ulong.
            int n = text.Length;
            int m = pattern.Length;
            int mc = Math.Min(8, m);

            // There can be no valid shifts with a pattern bigger than the text.
            if (m > n)
                return validShifts;

            // There can be no valid shifts with no pattern or no text.
            if (m == 0 || n == 0)
                return validShifts;

            // construct the mask and initial checksums.
            ulong ms = ConstructMask(pattern);
            ulong ts = InitialChecksum(text, mc);
            ulong ps = InitialChecksum(pattern, mc);

            // go through the input and find all valid shifts.
            for (int o = 0, l = n - m; o <= l; o++)
            {
                // apply the mask, in case our pattern is shorter than 8 bytes.
                ulong r = ts & ms;

                // check whether the sliders are equal.
                if (r == ps)
                {
                    // check the entire pattern at the given offset - in case our pattern is longer than 8 bytes.
                    if (CheckShift(text, pattern, o))
                    {
                        validShifts.Add(o);
                    }
                }

                if (o < l)
                {
                    // push the next byte into the input slider.
                    ts = PushByte(ts, text[o + mc]);
                }
            }
            
            return validShifts;
        }

        /// <summary>
        /// Constructs the mask for the given target. All bytes of the target will become 1's in the mask. All bytes missing (e.g., when the target is smaller than 8 bytes) will have 0's.
        /// </summary>
        private ulong ConstructMask(byte[] target)
        {
            ulong checksum = 0;
            int m = target.Length;
            int mc = Math.Min(8, m);

            for (int j = 0; j < mc; j++)
            {
                checksum = PushByte(checksum, (byte)255);
            }

            return checksum;
        }

        /// <summary>
        /// Constructs the initial checksum. Uses up to the first 8 bytes.
        /// </summary>
        private ulong InitialChecksum(byte[] target, int l)
        {
            ulong checksum = 0;

            for (int j = 0; j < l; j++)
            {
                checksum = PushByte(checksum, target[j]);
            }
            
            return checksum;
        }

        /// <summary>
        /// Adds the byte to the given checksum.
        /// </summary>
        /// <param name="checksum">The checksum that will receive the new byte.</param>
        /// <param name="b">The byte to add to the checksum.</param>
        /// <returns>The updated checksum with the byte pushed onto it.</returns>
        private ulong PushByte(ulong checksum, byte b)
        {
            return (checksum << 8) + (ulong)b;
        }
    }
}