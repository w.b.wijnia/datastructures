﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Algos.Search
{
    public class RabinKarp : Matching
    {

        public override List<int> Match(byte[] text, byte[] pattern)
        {
            // keeps track of the number of valid shifts found.
            List<int> validShifts = new List<int>();

            // retrieve the lengths.
            int n = text.Length;
            int m = pattern.Length;

            // There can be no valid shifts with a pattern bigger than the text.
            if (m > n)
                return validShifts;

            // There can be no valid shifts with no pattern.
            if (m == 0 || n == 0)
                return validShifts;

            int q = 97;                     // the modulos to use. Should be a primary number.
            int d = 256;                    // the set of possibilities for a single element. (for a byte this is 256)
            int h = PrepareH(d, q, m);      

            int ps = 0;                     // the slider value of the pattern.
            int ts = 0;                     // the slider value of the text.

            // prepare the slider values. Take note that they should not be
            // to be longer than the length of the pattern.
            for (int j = 0; j < m; j++)
            {
                ps = (d * ps + (int)pattern[j]) % q;
                ts = (d * ts + (int)text[j]) % q;
            }

            // slide over the text, find valid shifts.
            for(int j = 0, l = n - m; j <= l; j++)
            {
                // check the 'hashes'.
                if (ts == ps)
                {
                    // check whether it's not a false positive.
                    if (CheckShift(text, pattern, j))
                    {
                        validShifts.Add(j);
                    }
                }

                if (j < l)
                {
                    // compute the old that will be (rep)placed with a new value.
                    int old = d * (ts - text[j] * h);
                    int rep = text[j + m];

                    // remove the old (take note of the - in the expression of the definition of old),
                    // add in the new value and modulo the result. The modulo ensures the number does not
                    // explode when we work with bigger patterns. This step allows us to 'slide' over
                    // the given text.
                    ts = (old + rep) % q;

                    // the modulo operator reduces into the [-q, q] range. We need positive numbers,
                    // if it is negative we add the modulo. The result will always be positive.
                    if (ts < 0)
                    {
                        ts += q;
                    }
                }
            }

            return validShifts;
        }

        private int PrepareH(int d, int q, int m)
        {
            int h = 1;

            for (int j = 0; j < m - 1; j++)
            { h = (h * d) % q; }

            return h;
        }
    }
}