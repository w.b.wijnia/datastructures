﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Algos.Search
{
    public class Naive : Matching
    {

        public override List<int> Match(byte[] text, byte[] pattern)
        {
            // keeps track of the number of valid shifts found.
            List<int> validShifts = new List<int>();

            // retrieve the lengths.
            int n = text.Length;
            int m = pattern.Length;

            // There can be no valid shifts with a pattern bigger than the text.
            if (m > n)
                return validShifts;

            // There can be no valid shifts with no pattern.
            if (m == 0 || n == 0)
                return validShifts;

            // go through the input and find all valid shifts.
            for (int j = 0, l = n - m + 1; j < l; j++)
            {
                if (CheckShift(text, pattern, j))
                {
                    validShifts.Add(j);
                }
            }
            
            return validShifts;
        }
    }
}