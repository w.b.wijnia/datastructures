﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Algos.Search
{
    public abstract class Matching
    {

        protected byte[] input;

        /// <summary>
        /// Constructs a matching algorithm with no pre-defined input.
        /// </summary>
        public Matching() { }

        /// <summary>
        /// Searches the given pattern in the given text. The offset for every valid shift (hit) is returned.
        /// </summary>
        /// <param name="text">The text to search through.</param>
        /// <param name="pattern">The pattern to search for.</param>
        public abstract List<int> Match(byte[] text, byte[] pattern);

        /// <summary>
        /// Checks whether the given pattern is found starting at the given offset in the given text.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="pattern"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        protected bool CheckShift(byte[] text, byte[] pattern, int offset)
        {
            // retrieve the length of the target.
            int m = pattern.Length;

            // search through the input with offset 'o'.
            for (int j = 0; j < m; j++)
            {
                if (text[offset + j] != pattern[j])
                {
                    return false;
                }
            }

            // if all match, we return true.
            return true;
        }

    }
}
