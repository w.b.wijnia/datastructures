﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Algos.Sort.Array
{
    public class Bucket : Sorting<int>
    {
        /// <summary>
        /// The number of buckets.
        /// </summary>
        private const int nrOfBuckets = 10;

        public override int[] Sort(int[] array)
        {
            int n = array.Length;
            if (n <= 1)
                return array;

            // determine the bounds.
            int max = base.Maximum(array);
            int min = base.Minimum(array);

            throw new NotImplementedException();
                
            return array;
        }

        private int BucketNumber(int max, int min, int element)
        {
            return (element - min) / ((max - min) / nrOfBuckets);
        }
    }
}