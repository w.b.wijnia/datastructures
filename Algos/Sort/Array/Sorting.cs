﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Algos.Sort.Array
{
    public abstract class Sorting<T> where T:IComparable<T>
    {

        public abstract T[] Sort(T[] array);

        /// <summary>
        /// Swaps the given indices in the array.
        /// </summary>
        /// <param name="array">The array to perform the swap in.</param>
        /// <param name="a">The index to swap.</param>
        /// <param name="b">The index to swap.</param>
        protected void Swap (T[] array, int a, int b)
        {
            T temp = array[a];
            array[a] = array[b];
            array[b] = temp;
        }

        protected T Maximum(T[] array)
        {
            int n = array.Length;

            T h = array[0];
            for(int j = 1; j < n; j++)
            {
                T e = array[j];
                if(h.CompareTo(e) < 0)
                {
                    h = e;
                }
            }

            return h;
        }

        protected T Minimum(T[] array)
        {
            int n = array.Length;

            T l = array[0];
            for (int j = 1; j < n; j++)
            {
                T e = array[j];
                if (l.CompareTo(e) > 0)
                {
                    l = e;
                }
            }

            return l;
        }
    }
}