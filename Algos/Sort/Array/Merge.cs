﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Algos.Sort.Array
{
    public class Merge<T> : Sorting<T> where T : IComparable<T>
    {
        public override T[] Sort(T[] array)
        {
            // defensive programming
            if (array == null)
                throw new ArgumentNullException("Given array is null.");

            // if we only have one element, return it.
            int n = array.Length;
            if (n <= 1)
                return array;

            // split up the array into partitions.
            T[] p1, p2;
            int m = n / 2;

            p1 = new T[m];
            p2 = new T[n - m];

            CopyArray(array, p1, 0, m);
            CopyArray(array, p2, m, n);

            // sort the partitions.
            this.Sort(p1);
            this.Sort(p2);

            // combine the partitions.
            int cp1 = 0, cp2 = 0;
            for(int j = 0; j < n; j++)
            {
                // happens when the partition is empty.
                if(cp1 >= m)
                {
                    array[j] = p2[cp2];
                    cp2 = cp2 + 1;

                    continue;
                }

                // happens when the partition is empty.
                if (cp2 >= (n - m))
                {
                    array[j] = p1[cp1];
                    cp1 = cp1 + 1;

                    continue;
                }

                // if neither are empty, find the smallest element. Add that element to the array.
                T tp1 = p1[cp1];
                T tp2 = p2[cp2];

                if( tp1.CompareTo(tp2) < 0)
                {
                    array[j] = tp1;
                    cp1 = cp1 + 1;
                }
                else
                {
                    array[j] = tp2;
                    cp2 = cp2 + 1;
                }
            }

            return array;
        }

        private void CopyArray(T[] o, T[] d, int l, int h)
        {
            for (int j = l; j < h; j++)
                d[j - l] = o[j];
        }
    }
}