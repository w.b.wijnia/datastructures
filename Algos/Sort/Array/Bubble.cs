﻿using System;

namespace Algos.Sort.Array
{
    public class BubbleSort<T> : Sorting<T> where T: IComparable<T> 
    {
        public override T[] Sort(T[] array)
        {
            // defensive programming - is our input valid?
            if (array == null)
                throw new ArgumentNullException("Given array is null.");

            int n = array.Length;
            if (n <= 1)
                return array;

            // bubble sort!
            for(int j = 0; j < n; j++)
            {
                // assume the current element is the lowest.
                int ind = j;
                T l = array[j];

                // find a lower element.
                for (int i = j + 1; i < n; i++)
                {
                    // keep track of the lower element.
                    T c = array[i];
                    if (l.CompareTo(c) > 0)
                    {
                        ind = i;
                        l = c;
                    }
                }

                // swap the two elements - even if no longer element was found.
                // if no lower element is found, the element is swapped with itself.
                base.Swap(array, j, ind);
            }

            return array;
        }
    }
}