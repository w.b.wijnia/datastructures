﻿using System;

namespace Algos.Sort.Array
{
    public class Insertion<T> : Sorting<T> where T : IComparable<T>
    {
        public override T[] Sort(T[] array)
        {
            // defensive programming - is our input valid?
            if (array == null)
                throw new ArgumentNullException("Given array is null.");

            int n = array.Length;
            if (n <= 1)
                return array;

            // insertion sort!
            for (int j = 1; j < n; j++)
            {
                StoreElement(array, 0, j, j);
            }

            return array;
        }

        /// <summary>
        /// Stores the element (i) in the ordered section (l to h) of array (a).
        /// </summary>
        /// <param name="a">The array to place the given index at.</param>
        /// <param name="l">The lowerbound of the area where the index needs to be put.</param>
        /// <param name="h">The higherbound of the area where the index needs to be put.</param>
        /// <param name="i">The index to place.</param>
        private void StoreElement(T[] a, int l, int h, int i)
        {
            // keep track of the element we want to store.
            T target = a[i];

            // find out where we need to store it. As long as the element we find is smaller
            // than our target, we keep searching.
            int j = l;
            T element = a[j];
            while (target.CompareTo(element) > 0)
            {
                j = j + 1;
                element = a[j];

                // we cannot search beyond the ordered section.
                if (j >= h)
                    break;
            }

            // shift all of the elements one place to the right to make space.
            ShiftElements(a, j, (h - j));

            // store the element where it belongs.
            a[j] = target;
        }
        
        /// <summary>
        /// Shifts the elements (c) in array (a) from the given index (l) one to the right.
        /// </summary>
        /// <param name="a">The array to perform the shift in. </param>
        /// <param name="l">The index where the shifting starts.</param>
        /// <param name="c">The number of elements to shift one to the right.</param>
        private void ShiftElements(T[]a, int l, int c)
        {
            // shift the elements. Take note that if c == 0, no elements are shifted.
            for (int j = c + l; j > l; j--)
                a[j] = a[j - 1];
        }
    }
}