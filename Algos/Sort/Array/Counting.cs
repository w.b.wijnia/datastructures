﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Algos.Sort.Array
{
    public class Count : Sorting<int>
    {
        public override int[] Sort(int[] array)
        {
            // array of length 1 is already sorted.
            int n = array.Length;
            if (n <= 1)
                return array;

            // determine the bounds.
            int max = base.Maximum(array);
            int min = base.Minimum(array);

            // in case we have a negative bound.
            int offset = 0;
            if(min < 0)
            {
                offset = -min;
            }

            // construct the occurences table.
            int[] occurences = new int[max - min + 1];
            for(int j = 0; j < n; j++)
            {
                occurences[array[j] + offset - min] += 1;
            }

            // reconstruct the initial array.
            int c = 0;
            int m = occurences.Length;
            for(int j = 0; j < m; j++)
            {
                int o = occurences[j];

                for(int i = 0; i < o; i++)
                {
                    array[c] = j;
                    c += 1;
                }
            }

            return array;
        }
    }
}